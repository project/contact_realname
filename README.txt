
Contact real name module README

This module alters contact forms to display real name instead of
usernames.

See INSTALL.txt for instructions on how to install the module.

You can set which profile field contains the real name in the module
settings, as well as enabling the module for the site-wide contact form
and the user contact form.

